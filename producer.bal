import ballerinax/kafka;
import ballerina/io;
import ballerina/random;
import ballerinax/mongodb;
import ballerina/log;

public isolated function generateOrderID(string customerid, string storeid, int storeOrder) returns string|error{
    int randomNumber = check random:createIntInRange(1000, 9999);
    return string`R${storeid}-${customerid}-${storeOrder}-${randomNumber}`;
}


kafka:ProducerConfiguration producerConfig = {
    clientId: "order-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer productProducer = check new (kafka:DEFAULT_URL, producerConfig);

public function main() returns error? {

    Configuration config = {
        host: "localhost",
        port: 2181,
        database: "store",
        collection: "Customer"
    };

    Store store = new (config);

    string id = io:readln("Enter your Account ID Number : ");
    string password = io:readln("Enter your Password : ");
    boolean keepOrdering = true;
    Product[] products = [];
    decimal totalPrice = 0.0;
    int totalQuantity = 0;
    Product[] productList = [];
 
    if (check store.loginOn(id, password)) {
        io:println("Login Successful");
        io:println("Welcome to the Store");
        io:println("Select the store you would like to order from");

        while (keepOrdering) {
            string productId = io:readln("Enter the product ID : ");
            int|error quantity = check int:fromString(io:readln("Enter the quantity : "));

            if quantity is error {
                io:println("Invalid quantity");
            } else {
                products.push(product);
            }

            string keepOrderingString = io:readln("Do you want to place another order? (y/n) : ");
            if (keepOrderingString == "n") {
                keepOrdering = false;
            }
        }

        string confirmation = io:readln("Do you want to confirm your order? (y/n) : ");
        if (confirmation == "y") {
            Order customerOrder = {
                id: check generateOrderID(id, "", 0),
                customerId: id,
                products: products,
                totalQuantity: totalQuantity,
                totalPrice: totalPrice,
                status: "PLACED"
            };
            
            check store.saveOrder(customerOrder);
            io:println("Order Placed");

        } else {
            io:println("Login Failed");
        }
    }




public class Store {
    private mongodb:Client mongoClient;
    private string collection;

    public function init(Configuration configuration) {
        mongodb:ConnectionConfig mongoConfig = {
            host: configuration.host,
            port: configuration.port
        };
        self.mongoClient = checkpanic new (mongoConfig, configuration.database);
        self.collection = configuration.collection;
    }

    public isolated function loginOn(string id, string password) returns boolean|error {
        CustomerQuery customer = {
            id: id,
            password: password
        };

        stream<Customer, error?> result = checkpanic self.mongoClient->find(self.collection, (), customer, 'limit = 1);

        record {|Customer value;|}|error? res = result.next();

        while (res is record {|Customer value;|}) {
            Customer cust = res.value;
            if (id == cust.id && password == cust.password) {
                return true;
            }
        }
        return error("Invalid login credentials");
    }
 
    public isolated function getProducts() returns Product[] {
        stream<Product, error?> products = checkpanic self.mongoClient->find("Product", (), ());
        Product[] product = [];
        record {|Product value;|}|error? res = products.next();
        while (res is record {|Product value;|}) {
            product.push(res.value);
            res = products.next();
        }

        return product;
    }

    
    public function save(OrderConsumerRecord customerOrder) {
        
        map<json> _ = <map<json>>customerOrder.value.toJson();
    }
}

public enum Order_Status {
    PLACED,
    APPROVED,
    DELIVERED
}

public type Product record {|
    string id;
    string name;
    decimal price;
    int quantity;
|};

public type Order record {|
    string id;
    string customerId;
    Product[] products;
    int totalQuantity;
    decimal totalPrice;
    Order_Status status;

|};

public type Customer record {|
    string id;
    string name;
    string email;
    string phone;
    string address;
    string password;
    Order[] orders;
|};
public type CustomerQuery record {|
    string id;
    string password;
|};
 

public type Configuration record {|
    string host;
    int port;
    string database;
    string collection;
|};


public type ProductConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Product value;
|};
public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Order value;
|};


kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "consumer-group-id-1",
    topics: ["q-daddy-dsa-topic-1"],
    autoCommit: false,
    pollingInterval: 1
};

     Configuration databaseConfig = {
        host: "localhost",
        port: 2181,
        database: "test",
        collection: "Consumer_Prod"
    };