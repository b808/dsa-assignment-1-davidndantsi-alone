import ballerina/http;
import ballerina/io;

public function main() {
    io:println("Hello, World!");
}

type StudentRecord record {|

    readonly string studentNumber;
    string name ;
    string emailAdress;
    string course;
    string courseCode;
    string assessment;
    decimal marks;

|};

type ConflictingStudentNumberError record {|

 *http:Conflict;
  ErrorMsg body;

|};

type ErrorMsg record {|

 string errMsg;
|};


type StudentRecordCreated record {|
*http:Created;
StudentRecord[] body;

|};



type StudentRecordNotFound record {|

*http:NotFound;
string errMsg;

|};






 isolated table <StudentRecord> key (studentNumber) studentTable = table[

  {studentNumber : "219068119", name: "David Ndantsi",  emailAdress: "dndantsi@gmail.com", course: "Ethics for Computing",courseCode: "EFC621S", assessment: "EFC", marks: 90}

 ];

isolated service /Nust/StudentsRecord on new http:Listener(8080){


      isolated resource function post studentsIn(@http:Payload StudentRecord[] studentEntries) 
      returns StudentRecordCreated|ConflictingStudentNumberError {

           lock{

                string[] conflictingStudentNumb = from StudentRecord studentRecorded in studentEntries.clone()
                 where studentTable.clone().hasKey(studentRecorded.studentNumber) 
                 select studentRecorded.studentNumber;


                 if conflictingStudentNumb.length() >0 {


                        return <ConflictingStudentNumberError>{
                            body:{

                            
                               errMsg: "There was an Error!!! "


                            }
                        };

                 }
                 else {

                     studentEntries.clone().forEach( studentRecorded => studentTable.clone().add( studentRecorded));
                     return <StudentRecordCreated> {
                        body:studentEntries.clone()};
                     }
                 
                 }

           }


      }




isolated function 'join(string s, string s1, any c) returns string {
    return "";
}




