
import ballerina/graphql;
import ballerina/http;


public type CovidEntry record {|
readonly string isoCode;
string date;
string region;
decimal deaths;
decimal confirmed_cases;
decimal recoveries;
decimal tested;
|};

isolated table<CovidEntry> key(isoCode) covidEntriesTable = table[
{isoCode: "NAM", date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200}
];



 isolated  distinct service class CovidData {

    private final readonly& CovidEntry entryRecord;

    isolated  function init(CovidEntry entryRecord) {

         self.entryRecord = entryRecord.cloneReadOnly();
        
    }

    isolated  resource function get  isoCode () returns string{

        lock {
            return  self.entryRecord.isoCode;
        }

        
        
    }
    
    isolated  resource function get date() returns  string{

      lock{

        return self.entryRecord.date ;

      }         
    }


    isolated  resource function get region () returns string{

        lock {

              return self.entryRecord.region;

        }
        
    }

    isolated  resource function get deaths () returns decimal?{



        lock{

            if self.entryRecord.deaths is decimal{
            return self.entryRecord.deaths / 1000;
        }
      

        }  
    }

       

    isolated  resource function get recovered () returns decimal?{

        lock {

            if self.entryRecord. recoveries is decimal{
            return self.entryRecord. recoveries / 1000;
        }
       

        }
        
       
    }

     isolated resource function get tested () returns decimal?{


        lock {

        if self.entryRecord.tested is decimal{
            return self.entryRecord.tested / 1000;
        }
     

        }  
    }


   
 isolated resource function get confirmed_cases() returns decimal?{


        lock {

        if self.entryRecord.confirmed_cases is decimal{
            return self.entryRecord.confirmed_cases / 1000;
        }
      

        }  
    }


} 




listener http:Listener httpListener = check new(9000);
isolated service /covid19 on new graphql:Listener(httpListener) {



 isolated resource function get all() returns CovidData[]{

    lock{
        CovidEntry[] covidEntries = covidEntriesTable.clone().toArray().cloneReadOnly();
         return covidEntries.clone().map(entry => new CovidData(entry));
    }
 
 }

  isolated resource  function get filter(string isoCode) returns CovidData?{


       lock {

            CovidEntry? covidEntry = covidEntriesTable[isoCode].clone();
            if covidEntry is CovidEntry{
                return new (covidEntry);
            }
            return;

       }
}

 isolated remote function add(CovidEntry entry) returns CovidData?{


        lock {
              covidEntriesTable.clone().add(entry.clone());
              return new CovidData(entry.clone());  
        }
    }
}   



