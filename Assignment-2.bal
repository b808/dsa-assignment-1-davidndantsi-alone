import ballerinax/mongodb;
//import ballerina/io;
import ballerina/log;

final string register_collection = "Register";
final string store_collection = "Stores";
final string Items_collection = "Items";
final string database = "DSA-ASSIGNMENT1";

mongodb:ConnectionConfig mongoConfig = {
host: "localhost",
port: 27017,
options: {sslEnabled: false, serverSelectionTimeout: 5000}
};
mongodb:Client mongoClient = check new (mongoConfig, database);

// Main Program ran here.  
public function main() {
    Register customer = new();
    string customerResult = customer.addCustomer({username: "David", password: "1234", email_address: "dndantsi@gmail.com"});
    log:printInfo(customerResult);

    string customerVerification = customer.verifyCustomer("Valladolid", "1234");
    log:printInfo(customerVerification);

     mongoClient->close();
}
public class Register {

    public function addCustomer(Customer user) returns string {  
        //write to database
            do {
                check mongoClient->insert(user, register_collection);
                return string `Successfully added ${user.username}`;
            } on fail var e {
                return string `Failed to add ${user.username}. ` + e.toString();
            }
    } 

  public function verifyCustomer(string username, string password) returns string {
        // search for "filter words" and return response
        do {
            map<json> findDoc = {username: username,password: password};
            stream<Customer, error?> result = check mongoClient->find(register_collection,filter = findDoc);
            return "Search results:" + result.toString();
        } on fail var e {
            return "No results where found. " + e.toString();
        }
 }
}

